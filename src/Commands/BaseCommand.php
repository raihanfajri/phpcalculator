<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;

class BaseCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;

    public function __construct()
    {
        $this->signature = $this->getCommandSignature();
        $this->description = $this->getCommandDescription();
        
        parent::__construct();
    }

    protected function getCommandVerb(): string
    {
        return 'not implemented';
    }

    protected function getCommandPassiveVerb(): string
    {
        return 'not implemented';
    }

    protected function getOperator(): string
    {
        return 'not implemented';
    }

    protected function getCommandDescription(): string
    {
        $commandVerb = $this->getCommandVerb();

        return sprintf('%s all given Numbers', ucfirst($commandVerb));
    }

    protected function getCommandOptionPairs(): array
    {
        return [];
    }

    protected function getArgumentPairs(): array
    {
        $description = sprintf('The numbers to be %s', $this->getCommandPassiveVerb());

        return [
            'numbers' => [
                'description' => $description,
                'is_array' => true
            ]
        ];
    }

    protected function getCommandSignature(): string
    {
        $optionPairs = $this->getCommandOptionPairs();
        $argumentPairs = $this->getArgumentPairs();
        $argOpt = '';
        $usageDescription = '';

        foreach ($argumentPairs as $arg => $option)
        {
            $description = $option;
            if (gettype($option) === "array")
            {
                $description = $option['description'];

                if ($option['is_optional']) {
                    $argOpt .= '?';
                }

                if ($option['is_array']) {
                    $argOpt .= '*';
                }
            }

            $usageDescription .= sprintf(
                '{%s : %s}',
                $arg . $argOpt,
                $description
            );
        }

        foreach ($optionPairs as $opt => $description)
        {
            $usageDescription .= sprintf(
                '{%s : %s}',
                $opt,
                $description
            );
        }

        return sprintf('%s %s', $this->getCommandVerb(), $usageDescription);
    }

    public function handle(): void
    {
        $numbers = $this->getInput();
        $description = $this->generateCalculationDescription($numbers);
        $result = $this->calculateAll($numbers);

        $this->comment(sprintf('%s = %s', $description, $result));
    }

    protected function getInput(): array
    {
        $inputs = array();
        $argumentPairs = $this->getArgumentPairs();

        foreach ($argumentPairs as $arg => $description)
        {
            $input = $this->argument($arg);

            if (gettype($input) === "array") {
                $inputs = array_merge($inputs, $input);
            } else {
                $inputs[] = $input;
            }
        }

        return $inputs;
    }

    protected function generateCalculationDescription(array $numbers): string
    {
        $operator = $this->getOperator();
        $glue = sprintf(' %s ', $operator);

        return implode($glue, $numbers);
    }

    /**
     * @param array $numbers
     *
     * @return float|int
     */
    protected function calculateAll(array $numbers)
    {
        $number = array_pop($numbers);

        if (count($numbers) <= 0) {
            return $number;
        }

        return $this->calculate($this->calculateAll($numbers), $number);
    }

    /**
     * @param int|float $number1
     * @param int|float $number2
     *
     * @return int|float
     */
    protected function calculate($number1, $number2)
    {
        return 0;
    }
}
