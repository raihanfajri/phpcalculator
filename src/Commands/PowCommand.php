<?php

namespace Jakmall\Recruitment\Calculator\Commands;

class PowCommand extends BaseCommand
{
    protected function getCommandVerb(): string
    {
        return 'pow';
    }

    protected function getOperator(): string
    {
        return '^';
    }

    protected function getCommandDescription(): string
    {
        return 'Exponent the given number';
    }

    protected function getArgumentPairs(): array
    {
        return [
            'base' => 'The base number',
            'exp' => 'The exponent number'
        ];
    }

    /**
     * @param int|float $number1
     * @param int|float $number2
     *
     * @return int|float
     */
    protected function calculate($number1, $number2)
    {
        return $number1 ** $number2;
    }
}
